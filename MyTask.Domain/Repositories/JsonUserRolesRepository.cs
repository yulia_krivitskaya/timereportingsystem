﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyTask.Domain.Models;
using MyTask.DataAccess.JsonParsing;
using AutoMapper;

namespace MyTask.Domain.Repositories
{
    public class JsonUserRolesRepository : IUserRolesRepository
    {
        private readonly JsonDataContext _dataContext;

        public JsonUserRolesRepository(JsonDataContext dataContext)
        {
            _dataContext = dataContext;
        }
        public IEnumerable<Role> GetUserRoles(Guid userId)
        {
            if (!_dataContext.IsInitialized) _dataContext.Initialize();

            return _dataContext.UserRoles.Where(ur => ur.UserId == userId).Select(ur => (Role)ur.RoleId);
        }
    }
}
