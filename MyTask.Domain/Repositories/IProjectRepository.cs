﻿using MyTask.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyTask.Domain.Repositories
{
   public interface IProjectRepository
    {
        IEnumerable<Project> GetProjects();
        void UpdateProject(Project project);
        void CreateProject(Project project);

    }
}
