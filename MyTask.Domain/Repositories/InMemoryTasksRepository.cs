﻿using AutoMapper;
using MyTask.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyTask.Domain.Repositories
{
    public class InMemoryTasksRepository: ITasksRepository
    {
        private static readonly List<Tasks> AllTasks = new List<Tasks>
        {
            new Tasks
            {
                Id=new Guid("1755537E-DD19-47FA-A7B8-6069493DC5F0"),
                ProjectId = new Guid("A5C22515-E56A-4D1C-9A2C-9D48CD87772D"),
                Name="Task 1",
                Description= "Answer question",
                StartDate=new DateTime(2015,09,01),
                EndDate=new DateTime(2015,09,11),
                State= State.Draft
             

            },
            new Tasks
            {
                Id=Guid.NewGuid(),
                ProjectId = new Guid("A5C22515-E56A-4D1C-9A2C-9D48CD87772D"),
                Name="Task 2",
                Description= "Answer question 2",
                StartDate=new DateTime(2015,11,01),
                EndDate=new DateTime(2015,11,13),
                State=State.Completed

            }
        };

        public IEnumerable<Tasks> ShowAllTasks()
        {
            return AllTasks;
        }
        public IEnumerable<Tasks> GetTasks(Guid projectId)
            {
            return AllTasks.Where(p => p.ProjectId == projectId);
            }
        public void UpdateTask(Tasks task)
        {
            var exitingTasks = AllTasks.FirstOrDefault(t => t.Id == task.Id);

            if (exitingTasks == null) throw new InvalidOperationException();

            Mapper.Map(task, exitingTasks);
        }
        public void CreateTask(Tasks task)
        {
            task.Id = Guid.NewGuid();
            AllTasks.Add(task);
        }
    }
    
}
