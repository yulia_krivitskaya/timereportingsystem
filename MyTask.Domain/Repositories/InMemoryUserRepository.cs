﻿using MyTask.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyTask.Domain.Repositories
{
 public class InMemoryUserRepository : IUserRepository
    {
        private static readonly List<User> Users = new List<User>
        {
            new User
            {
                Id = new Guid("5DFA945C-639A-4578-87F7-355C7C0C5335"),
                Password = "admin",
                UserName = "Admin",
                FirstName = "Michael",
                LastName ="Merc",
                Locale ="en",
                TimeZone =TimeZoneInfo.Local.Id,
                RegistrationDate = DateTime.Now,
                Role = Role.Admin


            },
            new User
            {
                Id = new Guid("7459B9C7-133B-4233-8945-2D763D982721"),
                Password = "user",
                UserName = "User",
                FirstName = "Michael",
                LastName ="Merc",
                Locale ="en",
                TimeZone =TimeZoneInfo.Local.Id,
                RegistrationDate = DateTime.Now,
                Role = Role.User
            }
        };

        public IEnumerable<User> GetUsers()
        {
            return Users;
        }

        public User GetById(Guid id)
        {
            return Users.FirstOrDefault(u => u.Id == id);
        }
    }
}
