﻿using MyTask.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyTask.Domain.Repositories
{
   public interface ITaskReportRepository
    {
        IEnumerable<TaskReport> GetAllReports(Guid tasksId);
        IEnumerable<TaskReport> ShowAllReportsForUser(Guid userid, Guid taskid);
        TaskReport GetTaskReport(Guid taskreportid);
        void CreateTaskReport(TaskReport taskReport);
        void UpdateTaskReport(TaskReport taskReport);
    }
}
