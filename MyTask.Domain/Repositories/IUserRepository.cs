﻿using MyTask.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyTask.Domain.Repositories
{
    public interface IUserRepository
    {
        IEnumerable<User> GetUsers();
        User GetById(Guid id);
    }
}