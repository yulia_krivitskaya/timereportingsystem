﻿using AutoMapper;
using MyTask.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyTask.Domain.Repositories
{
    public class InMemoryTaskReportRepository : ITaskReportRepository
    {
        private static readonly List<TaskReport> AllTaskReports = new List<TaskReport>
        {
            new TaskReport
            {
                Id=Guid.NewGuid(),
                UserId = new Guid("5DFA945C-639A-4578-87F7-355C7C0C5335"),
                TaskId = new Guid("1755537E-DD19-47FA-A7B8-6069493DC5F0"),
                Description= "My Answer question",
                CreatedDate= new DateTime(2015,11,02),
                EditedDate = new DateTime(2015,11,02),
                DateOfReport = new DateTime(2015,11,02),
                Effort = new TimeSpan (04,10,00)
            }

        };

        public IEnumerable<TaskReport> GetAllReports(Guid tasksId)
        {
            return AllTaskReports.Where(tr => tr.TaskId == tasksId);
        }
        public IEnumerable<TaskReport> ShowAllReportsForUser(Guid userid, Guid taskid)
        {
            return AllTaskReports.Where(tr => tr.UserId == userid && tr.TaskId == taskid);
        }

        public TaskReport GetTaskReport(Guid taskreportid)
        {
            return AllTaskReports.FirstOrDefault(tr => tr.Id== taskreportid);
        }

        public void CreateTaskReport(TaskReport taskReport)
        {
            taskReport.Id = Guid.NewGuid();
            taskReport.CreatedDate = DateTime.Now;
            taskReport.EditedDate = DateTime.Now;
            AllTaskReports.Add(taskReport);
        }
       public void UpdateTaskReport(TaskReport taskReport)
        {
            var exitingTaskReports = AllTaskReports.FirstOrDefault(t => t.Id == taskReport.Id);
            exitingTaskReports.EditedDate = DateTime.Now;

            if (exitingTaskReports == null) throw new InvalidOperationException();

            Mapper.Map(taskReport, exitingTaskReports);
        }
    }
}
