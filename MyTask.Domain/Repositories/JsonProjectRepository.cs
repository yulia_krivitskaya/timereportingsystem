﻿using AutoMapper;
using MyTask.DataAccess.JsonParsing;
using MyTask.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyTask.Domain.Repositories
{
   public class JsonProjectRepository : IProjectRepository
    {
        private readonly JsonDataContext _dataContext;

        public JsonProjectRepository (JsonDataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public IEnumerable<Project> GetProjects()
        {
            if (!_dataContext.IsInitialized) _dataContext.Initialize();

            return Mapper.Map<IEnumerable<Project>>(_dataContext.Projects);
        }
        public void UpdateProject(Project project)
        {
            if (!_dataContext.IsInitialized) _dataContext.Initialize();

            _dataContext.Projects.RemoveAll(p => p.Id == project.Id);
            var dataAccessModel = Mapper.Map<MyTask.DataAccess.Models.Project>(project);

            _dataContext.Projects.Add(dataAccessModel);
            _dataContext.SaveChanges();
        }
      public void CreateProject(Project project)
        {
            if (!_dataContext.IsInitialized) _dataContext.Initialize();
            project.Id = Guid.NewGuid();
            var dataModel = Mapper.Map<MyTask.DataAccess.Models.Project>(project);
            _dataContext.Projects.Add(dataModel);
            _dataContext.SaveChanges();

        }
    }
}
