﻿using MyTask.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyTask.Domain.Repositories
{
  public  class InMemoryUserRolesRepository: IUserRolesRepository
    {
        private static readonly List<Tuple<Guid, Role>> UserRoles = new List<Tuple<Guid, Role>>
        {
            new Tuple<Guid, Role>(new Guid("5DFA945C-639A-4578-87F7-355C7C0C5335"), Role.Admin),
            new Tuple<Guid, Role>(new Guid("5DFA945C-639A-4578-87F7-355C7C0C5335"), Role.Manager),
            new Tuple<Guid, Role>(new Guid("7459B9C7-133B-4233-8945-2D763D982721"), Role.User),
        };

        public IEnumerable<Role> GetUserRoles(Guid userId)
        {
            return UserRoles.Where(ur => ur.Item1 == userId).Select(ur => ur.Item2);
        }
    }
}
