﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyTask.Domain.Models;
using MyTask.DataAccess.JsonParsing;
using AutoMapper;

namespace MyTask.Domain.Repositories
{
    public class JsonTaskReportRepository : ITaskReportRepository
    {
        private readonly JsonDataContext _dataContext;

        public JsonTaskReportRepository(JsonDataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public IEnumerable<TaskReport> GetAllReports(Guid tasksId)
        {
            if (!_dataContext.IsInitialized) _dataContext.Initialize();

            return Mapper.Map<IEnumerable<TaskReport>>(_dataContext.TaskReports.Where(t => t.TaskId == tasksId));
        }

        public IEnumerable<TaskReport> ShowAllReportsForUser(Guid userid, Guid taskid)
        {
            if (!_dataContext.IsInitialized) _dataContext.Initialize();

            return Mapper.Map<IEnumerable<TaskReport>>(_dataContext.TaskReports.Where(tr => tr.UserId == userid && tr.TaskId == taskid));
        }

       
        public TaskReport GetTaskReport(Guid taskreportid)
        {
            if (!_dataContext.IsInitialized) _dataContext.Initialize();

            return Mapper.Map<TaskReport>(_dataContext.TaskReports.Where(t => t.Id== taskreportid));
        }

        public void CreateTaskReport(TaskReport taskReport)
        {
            if (!_dataContext.IsInitialized) _dataContext.Initialize();
            taskReport.Id = Guid.NewGuid();
            taskReport.CreatedDate = DateTime.Now;
            taskReport.EditedDate = DateTime.Now;
            var dataModel = Mapper.Map<MyTask.DataAccess.Models.TaskReport>(taskReport);
            _dataContext.TaskReports.Add(dataModel);
            _dataContext.SaveChanges();
        }
        public void UpdateTaskReport(TaskReport taskReport)
        {
            if (!_dataContext.IsInitialized) _dataContext.Initialize();

            _dataContext.TaskReports.RemoveAll(t => t.Id == taskReport.Id);
            var dataAccessModel = Mapper.Map<MyTask.DataAccess.Models.TaskReport>(taskReport);

            _dataContext.TaskReports.Add(dataAccessModel);
            _dataContext.SaveChanges();
        }
    }
}
