﻿using AutoMapper;
using MyTask.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyTask.Domain.Repositories
{
    public class InMemoryProjectRepository : IProjectRepository
    {
        private static readonly List<Project> Projects = new List<Project>
        {
            new Project
            {
                Id= new Guid("A5C22515-E56A-4D1C-9A2C-9D48CD87772D"),
                Name="Your First Project",
                Description= "You should pay your all attention",
                StartDate=new DateTime(2015,09,01),
                EndDate=new DateTime(2015,12,16)

            },
            new Project
            {
                Id=Guid.NewGuid(),
                Name="Your Second Project",
                Description= "You should pay your all attention",
                StartDate=new DateTime(2015,09,30),
                EndDate=new DateTime(2015,12,13)

            }
        };
        public IEnumerable<Project> GetProjects()
        {
            return Projects;
        }
        public void UpdateProject(Project project)
        {
            var exitingProject = Projects.FirstOrDefault(p => p.Id == project.Id);

            if (exitingProject == null) throw new InvalidOperationException();

            Mapper.Map(project, exitingProject);
        }

       public void CreateProject(Project project)
        {
            project.Id = Guid.NewGuid();
            Projects.Add(project);
        }
    }
}