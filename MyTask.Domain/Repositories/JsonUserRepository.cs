﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyTask.Domain.Models;
using MyTask.DataAccess.JsonParsing;
using AutoMapper;

namespace MyTask.Domain.Repositories
{
    public class JsonUserRepository : IUserRepository
    {
        private readonly JsonDataContext _dataContext;

        public JsonUserRepository(JsonDataContext dataContext)
        {
            _dataContext = dataContext;
        }
        public User GetById(Guid id)
        {
            if (!_dataContext.IsInitialized) _dataContext.Initialize();

            var dataAccessUser = _dataContext.Users.FirstOrDefault(u => u.Id == id);
            return Mapper.Map<User>(dataAccessUser);
        }

        public IEnumerable<User> GetUsers()
        {
            if (!_dataContext.IsInitialized) _dataContext.Initialize();

            return Mapper.Map<IEnumerable<User>>(_dataContext.Users);
        }
    }
}
