﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyTask.Domain.Models;
using MyTask.DataAccess.JsonParsing;
using AutoMapper;

namespace MyTask.Domain.Repositories
{
   public class JsonTasksRepository : ITasksRepository
    {
        private readonly JsonDataContext _dataContext;

        public JsonTasksRepository(JsonDataContext dataContext)
        {
            _dataContext = dataContext;
        }
        public IEnumerable<Tasks> GetTasks(Guid projectId)
        {
            if (!_dataContext.IsInitialized) _dataContext.Initialize();

            return Mapper.Map<IEnumerable<Tasks>>(_dataContext.Tasks.Where(t=>t.ProjectId==projectId));
        }

        public IEnumerable<Tasks> ShowAllTasks()
        {
            if (!_dataContext.IsInitialized) _dataContext.Initialize();

            return Mapper.Map<IEnumerable<Tasks>>(_dataContext.Tasks);
        }

        public void UpdateTask(Tasks task)
        {
            if (!_dataContext.IsInitialized) _dataContext.Initialize();

            _dataContext.Tasks.RemoveAll(t => t.Id == task.Id);
            var dataAccessModel = Mapper.Map<MyTask.DataAccess.Models.Tasks>(task);

            _dataContext.Tasks.Add(dataAccessModel);
            _dataContext.SaveChanges();
        }
        public void CreateTask(Tasks task)
        {
            if (!_dataContext.IsInitialized) _dataContext.Initialize();
            task.Id = Guid.NewGuid();
            var dataModel = Mapper.Map<DataAccess.Models.Tasks>(task);
            _dataContext.Tasks.Add(dataModel);
            _dataContext.SaveChanges();
        }
        }
}
