﻿using MyTask.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyTask.Domain.Repositories
{
   public interface ITasksRepository
    {
        IEnumerable<Tasks> ShowAllTasks();
        IEnumerable<Tasks> GetTasks(Guid projectId);
        void CreateTask(Tasks task);
        void UpdateTask(Tasks task);
    }
}
