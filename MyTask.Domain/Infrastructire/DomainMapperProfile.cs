﻿using AutoMapper;
using MyTask.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyTask.Domain.Infrastructire
{
    public class DomainMapperProfile: Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Project,Project>()
                .ForMember(pr => pr.Id, opt => opt.Ignore());
            Mapper.CreateMap<Tasks, Tasks>()
                .ForMember(t => t.Id, opt => opt.Ignore());
            Mapper.CreateMap<TaskReport, TaskReport>()
                .ForMember(tr => tr.Id, opt => opt.Ignore());

            Mapper.CreateMap<Project, MyTask.DataAccess.Models.Project>();
            Mapper.CreateMap< MyTask.DataAccess.Models.Project, Project>();

            Mapper.CreateMap<Tasks, MyTask.DataAccess.Models.Tasks>();
            Mapper.CreateMap<MyTask.DataAccess.Models.Tasks, Tasks>();

            Mapper.CreateMap<TaskReport, MyTask.DataAccess.Models.TaskReport>();
            Mapper.CreateMap<MyTask.DataAccess.Models.TaskReport, TaskReport>();

            Mapper.CreateMap<User, MyTask.DataAccess.Models.User>();
            Mapper.CreateMap<MyTask.DataAccess.Models.User, User>();

            Mapper.CreateMap<Role, DataAccess.Models.UserRole>();
            Mapper.CreateMap<DataAccess.Models.UserRole, Role>();

            Mapper.CreateMap<State, DataAccess.Models.State>();
            Mapper.CreateMap< DataAccess.Models.State, State>();
        }
}
}
