﻿using MyTask.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyTask.Domain.Services
{
   public interface ITaskReportService
    {
        IEnumerable<TaskReport> ShowAllReports(Guid taskId);

        IEnumerable<TaskReport> ShowAllReportsForUser(Guid userid, Guid taskId);
        TaskReport ShowTaskReport(Guid taskperortId);
        TaskReport EditTaskReport(Guid taskperortId);
        void CreateTaskReport(TaskReport taskReport);
        void EditTaskReport(TaskReport taskReport);
    }
}
