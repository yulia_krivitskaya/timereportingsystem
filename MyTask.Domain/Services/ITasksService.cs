﻿using MyTask.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyTask.Domain.Services
{
   public interface ITasksService
    {
        IEnumerable<Tasks> ShowTasks(Guid projectId);
        Tasks ShowTaskDecription(Guid taskId);
        IEnumerable<Tasks> EditTasks(Guid projectId);
        void EditTask(Tasks task);
        void Create(Tasks task);


    }
}
