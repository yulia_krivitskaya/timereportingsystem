﻿using MyTask.Domain.Models;
using MyTask.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyTask.Domain.Services
{
   public class TaskReportService: ITaskReportService
    {
        private readonly ITaskReportRepository _tasksReportRepository;

        public TaskReportService(ITaskReportRepository tasksReportRepository)
        {
            _tasksReportRepository = tasksReportRepository;
        }

       public IEnumerable<TaskReport> ShowAllReports(Guid taskId)
        {
            var reports = _tasksReportRepository.GetAllReports(taskId);
            return reports;
        }

        public IEnumerable<TaskReport> ShowAllReportsForUser(Guid userid, Guid taskId)
        {
            var reports = _tasksReportRepository.ShowAllReportsForUser(userid, taskId);
            return reports;
        }
        public TaskReport ShowTaskReport(Guid taskreportId)
        {
            var taskReport = _tasksReportRepository.GetTaskReport(taskreportId);
            return taskReport;
        }

        public TaskReport EditTaskReport(Guid taskreportId)
        {
            var taskReport = _tasksReportRepository.GetTaskReport(taskreportId);
            return taskReport;
        }
        public void CreateTaskReport(TaskReport taskReport)
        {
            _tasksReportRepository.CreateTaskReport(taskReport);
        }
        public void EditTaskReport(TaskReport taskReport)
        {
            if (taskReport == null) throw new ArgumentNullException(nameof(taskReport));

            _tasksReportRepository.UpdateTaskReport(taskReport);
        }
    }
}
