﻿using MyTask.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyTask.Domain.Services
{
    public interface IUserService
    {
       
        User ShowUser(Guid userId);
        
    }
}
