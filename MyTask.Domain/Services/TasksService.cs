﻿using MyTask.Domain.Models;
using MyTask.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyTask.Domain.Services
{
   public class TasksService : ITasksService
    {
        private readonly ITasksRepository _tasksRepository;

        public TasksService(ITasksRepository tasksRepository)
        {
            _tasksRepository = tasksRepository;
        }

        public IEnumerable<Tasks> ShowTasks(Guid projectId)
        {
            var tasks = _tasksRepository.GetTasks(projectId);
            return tasks;
        }
      public Tasks ShowTaskDecription(Guid taskId)
        {
            var showingTask = _tasksRepository.ShowAllTasks().FirstOrDefault(t => t.Id == taskId);

            if (showingTask == null) throw new ArgumentException("There is no projects with the specified id", nameof(taskId));

            return showingTask;
        }
       public IEnumerable<Tasks> EditTasks(Guid projectId)
        {
            var allTasks = _tasksRepository.GetTasks(projectId).ToList();
            return allTasks;
        }
       public void EditTask(Tasks task)
        {
            if (task == null) throw new ArgumentNullException(nameof(task));
            _tasksRepository.UpdateTask(task);

        }
        public void Create(Tasks task)
        {
            _tasksRepository.CreateTask(task);
        }
    }
}
