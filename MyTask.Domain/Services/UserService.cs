﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyTask.Domain.Models;
using MyTask.Domain.Repositories;

namespace MyTask.Domain.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;

        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }
        public User ShowUser(Guid userId)
        {
           return _userRepository.GetById(userId);
        }
    }
}
