﻿using MyTask.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyTask.Domain.Services
{
    public interface IProjectService
    {
        IEnumerable<Project> ShowAvaliableProjects();
        Project ShowProjectDecription(Guid projectId);
        IEnumerable<Project> EditProjects();
        void EditProject(Project project);
        void CreateProject(Project project);
    }
}
