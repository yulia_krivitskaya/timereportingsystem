﻿using MyTask.Domain.Models;
using MyTask.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyTask.Domain.Services
{
    public class ProjectService : IProjectService
    {
        private readonly IProjectRepository _projectRepository;
        public ProjectService(IProjectRepository projectRepository)
        {
            _projectRepository = projectRepository;
        }
        public IEnumerable<Project> ShowAvaliableProjects()
        {
            var avaliableProjects = _projectRepository.GetProjects().Where(pr => pr.EndDate >= DateTime.Now).ToList();
            return avaliableProjects;
        }
        public Project ShowProjectDecription(Guid projectId)
        {
            var showingProject = _projectRepository.GetProjects().FirstOrDefault(proj => proj.Id == projectId);

            if (showingProject == null) throw new ArgumentException("There is no projects with the specified id", nameof(projectId));

            return showingProject;
        }

        public IEnumerable<Project> EditProjects()
        {
            var allProjects = _projectRepository.GetProjects().ToList();
            return allProjects;
        }

        public void EditProject (Project project)
        {
            if (project == null) throw new ArgumentNullException(nameof(project));

            _projectRepository.UpdateProject(project);
        }

       public void CreateProject(Project project)
        {
            
            _projectRepository.CreateProject(project);
        }
    }
}
