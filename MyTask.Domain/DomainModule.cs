﻿using Autofac;
using MyTask.Domain.Repositories;
using MyTask.Domain.Services;

namespace MyTask.Domain
{
    public class DomainModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<ProjectService>().As<IProjectService>();
            builder.RegisterType<TasksService>().As<ITasksService>();
            builder.RegisterType<TaskReportService>().As<ITaskReportService>();
            builder.RegisterType<UserService>().As<IUserService>();

            builder.RegisterType<JsonProjectRepository>().As<IProjectRepository>();
            builder.RegisterType<JsonTaskReportRepository>().As<ITaskReportRepository>();
            builder.RegisterType<JsonTasksRepository>().As<ITasksRepository>();
            builder.RegisterType<JsonUserRepository>().As<IUserRepository>();
            builder.RegisterType<JsonUserRolesRepository>().As<IUserRolesRepository>();


            base.Load(builder);
        }
    }
}
