﻿using MyTask.DataAccess.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyTask.DataAccess.JsonParsing
{
    public class JsonDataContext 
    {
        
        private readonly string _fileName;
        public List<Project> Projects { get; set; }
        public List<State> States { get; set; }
        public List<Tasks> Tasks { get; set; }
        public List<TaskReport> TaskReports { get; set; }
        public List<User> Users { get; set; }
        public List<UserRole> UserRoles { get; set; }
        public bool IsInitialized { get; private set; }

        public JsonDataContext(string fileName)
        {
            _fileName = fileName;
            Projects = new List<Project>();
            Users = new List<User>();
            UserRoles = new List<UserRole>();
            States = new List<State>();
            Tasks = new List<Tasks>();
            TaskReports = new List<TaskReport>();
        }

        public void Initialize()
        {
            using (var sr = new StreamReader(_fileName))
            {
            var value = sr.ReadToEnd();
            var data = JsonConvert.DeserializeObject<JsonDataContext>(value);


                //    // При создании файла создаем админа
                //    User user = new User
                //    {
                //        Id = Guid.NewGuid(),
                //        UserName = "Admin",
                //        Password = "admin",
                //        FirstName = "admin",
                //        LastName = "adminovich",
                //        Locale = "en",
                //        RegistrationDate = DateTime.Now,
                //        TimeZone = TimeZoneInfo.Local.Id,

                //    };

                //    Users.Add(user);
                //    SaveChanges();
                //}


                Projects = data.Projects;
                Users = data.Users;
                UserRoles = data.UserRoles;
                States = data.States;
                Tasks = data.Tasks;
                TaskReports = data.TaskReports;

                IsInitialized = true;
            }
        }
        public void SaveChanges()
        {
            using (var sw = new StreamWriter(_fileName, false))
            {
                var value = JsonConvert.SerializeObject(this);
                sw.Write(value);
            }
        }

        public void InitializeWithFakeData()
        {
            Users.AddRange(new User[]
            {
                new User
            {
                Id = new Guid("5DFA945C-639A-4578-87F7-355C7C0C5335"),
                Password = "admin",
                UserName = "Admin"
            },
            new User
            {
                Id = new Guid("7459B9C7-133B-4233-8945-2D763D982721"),
                Password = "user",
                UserName = "User"
            }
            });
            var d = new JsonDataContext("~/App_Data/DataFile.json");
        }
    }
}
