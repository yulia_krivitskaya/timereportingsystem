﻿using Autofac;
using MyTask.DataAccess.JsonParsing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyTask.DataAccess
{
   public class DataAccessModule : Module  
    {
        private string connStr;
        public DataAccessModule(string connString)
        {
            this.connStr = connString;
        }
        protected override void Load(ContainerBuilder builder)
        {
            builder.Register(c=> new JsonDataContext(connStr))
             .AsSelf();

            base.Load(builder);
        }

    }
}
