﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyTask.DataAccess.Models
{
   public class State
    {
        public Guid TasksId { get; set; }
        public int StateId { get; set; }
    }
}
