﻿using Autofac;
using Microsoft.AspNet.Identity;
using MyTask.DataAccess.JsonParsing;
using MyTask.UI.Authentication;
using MyTask.UI.Models;
using System.Web;

namespace MyTask.UI
{
    public class UIModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<UserManager<UserViewModel, string>>().AsSelf();
            builder.RegisterType<CustomUserStore>().As<IUserStore<UserViewModel, string>>();
            builder.RegisterType<JsonDataContext>().WithParameter("fileName", HttpContext.Current != null ? HttpContext.Current.Server.MapPath("~/App_Data/Data.json") : null);
            base.Load(builder);
        }
    }
    }