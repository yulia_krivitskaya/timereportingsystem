﻿using Autofac;
using Autofac.Integration.Mvc;
using MyTask.DataAccess;
using MyTask.Domain;
using System.Web;
using System.Web.Mvc;

namespace MyTask.UI.Util
{
    public class AutofacConfig
    {
        public static void ConfigureContainer()
        {

            var builder = new ContainerBuilder();


            builder.RegisterControllers(typeof(MvcApplication).Assembly);


            
            builder.RegisterModule(new UIModule());
            builder.RegisterModule(new DataAccessModule(HttpContext.Current.Server.MapPath("~/App_Data/Data.json")));
            builder.RegisterModule(new DomainModule());

            var container = builder.Build();

            // установка сопоставителя зависимостей
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}