﻿using AutoMapper;
using MyTask.Domain.Models;
using MyTask.UI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyTask.UI.App_Start
{
    public class MapperProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Project, ProjectViewModel>();
            Mapper.CreateMap<ProjectViewModel, Project>();

            Mapper.CreateMap<UserViewModel, User>();
            Mapper.CreateMap<User, UserViewModel>();

            Mapper.CreateMap<Tasks, TasksViewModel>();
            Mapper.CreateMap<TasksViewModel, Tasks>();

            Mapper.CreateMap<TaskReport, TaskReportViewModel>();
            Mapper.CreateMap<TaskReportViewModel, TaskReport>();

            Mapper.CreateMap<FullUserViewModel, Domain.Models.User>();
            Mapper.CreateMap<Domain.Models.User, FullUserViewModel>();

          

            base.Configure();
        }
    }
}