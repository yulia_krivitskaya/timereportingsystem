﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using MyTask.Domain.Repositories;
using MyTask.UI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;


namespace MyTask.UI.Authentication
{
    public class CustomUserStore : IUserPasswordStore<UserViewModel, string>,
        IUserClaimStore<UserViewModel, string>
    {
        private readonly IUserRepository _userRepository;
        private readonly IUserRolesRepository _userRolesRepository;

        public CustomUserStore(IUserRepository userRepository, IUserRolesRepository userRolesRepository)
        {
            _userRepository = userRepository;
            _userRolesRepository = userRolesRepository;
        }

        public void Dispose()
        {
        }

        public Task CreateAsync(UserViewModel user)
        {
            throw new NotImplementedException();
        }

        public Task UpdateAsync(UserViewModel user)
        {
            throw new NotImplementedException();
        }

        public Task DeleteAsync(UserViewModel user)
        {
            throw new NotImplementedException();
        }

        public Task<UserViewModel> FindByIdAsync(string userId)
        {
            var user = _userRepository.GetById(Guid.Parse(userId));

            var viewModel = Mapper.Map<UserViewModel>(user);

            return Task.FromResult(viewModel);
        }

        
        public Task<UserViewModel> FindByNameAsync(string userName)
        {
            var user = _userRepository.GetUsers()
                .FirstOrDefault(u => u.UserName.Equals(userName, StringComparison.OrdinalIgnoreCase));

            var viewModel = Mapper.Map<UserViewModel>(user);

            return Task.FromResult(viewModel);
        }

        public Task SetPasswordHashAsync(UserViewModel user, string passwordHash)
        {
            throw new NotImplementedException();
        }

        public Task<string> GetPasswordHashAsync(UserViewModel user)
        {
            var stored = _userRepository.GetById(Guid.Parse(user.Id));

            return Task.FromResult(new PasswordHasher().HashPassword(stored.Password));
        }

        public Task<bool> HasPasswordAsync(UserViewModel user)
        {
            return Task.FromResult(true);
        }

        public Task AddToRoleAsync(UserViewModel user, string roleName)
        {
            throw new NotImplementedException();
        }

        public Task RemoveFromRoleAsync(UserViewModel user, string roleName)
        {
            throw new NotImplementedException();
        }

        public Task<IList<string>> GetRolesAsync(UserViewModel user)
        {
            throw new NotImplementedException();
        }

        public Task<bool> IsInRoleAsync(UserViewModel user, string roleName)
        {
            throw new NotImplementedException();
        }

        public Task<IList<Claim>> GetClaimsAsync(UserViewModel user)
        {
            var roles = _userRolesRepository.GetUserRoles(new Guid(user.Id));

            IList<Claim> claims = roles.Select(r => new Claim(ClaimTypes.Role, r.ToString())).ToList();
            return Task.FromResult(claims);
        }

        public Task RemoveClaimAsync(UserViewModel user, Claim claim)
        {
            throw new NotImplementedException();
        }

        public Task AddClaimAsync(UserViewModel user, Claim claim)
        {
            throw new NotImplementedException();
        }
    }
}
