﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyTask.UI.Models
{
    public class ProjectViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public TasksViewModel Task { get; set; }

    }
}