﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyTask.UI.Models
{
    public class FullUserViewModel
    {
        public string Id { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime RegistrationDate { get; set; }
        public string Locale { get; set; }
        public string TimeZone { get; set; }

        

    }
}