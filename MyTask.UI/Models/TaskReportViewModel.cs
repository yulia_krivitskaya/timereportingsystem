﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MyTask.UI.Models
{
    public class TaskReportViewModel
    {
        public Guid Id { get; set; }
        public Guid TaskId { get; set; }
        public Guid UserId { get; set; }

        [DataType(DataType.Date)]
        public DateTime DateOfReport { get; set; }

        [DataType(DataType.Date)]
        public DateTime CreatedDate { get; set; }

        [DataType(DataType.DateTime)]
        public DateTime EditedDate { get; set; }
        public string Description { get; set; }

        [DataType(DataType.Time)]
        public TimeSpan Effort { get; set; }
    }
}