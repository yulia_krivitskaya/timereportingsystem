﻿using AutoMapper;
using MyTask.Domain.Models;
using MyTask.Domain.Repositories;
using MyTask.Domain.Services;
using MyTask.UI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;

namespace MyTask.UI.Controllers
{
    public class TaskReportController : Controller
    {
        private readonly ITaskReportService _taskReportService;
        public TaskReportController(ITaskReportService taskReportService)
        {
            _taskReportService = taskReportService;
        }

        public PartialViewResult Index(Guid userId, Guid taskId)
        {
            var taskreportsList = _taskReportService.ShowAllReportsForUser(userId, taskId);

            var viewModel = Mapper.Map<IEnumerable<TaskReportViewModel>>(taskreportsList);

            return PartialView(viewModel);
        }

        public ActionResult Description(Guid reportId)
        {           
            var report = _taskReportService.ShowTaskReport(reportId);
            var viewModel = Mapper.Map<TaskReportViewModel>(report);

            return View(viewModel);
        }

        public ActionResult Create(Guid taskId)
        {
            var report = new TaskReportViewModel();
            report.TaskId = taskId;
            report.UserId = new Guid(User.Identity.GetUserId());
            return View(report);
        }
        [HttpPost]
        public ActionResult Create(TaskReportViewModel taskreport)
        {
            var newreport = Mapper.Map<TaskReport>(taskreport);
            _taskReportService.CreateTaskReport(newreport);
            return RedirectToAction("Description", "Tasks" , new { id = newreport.TaskId });
        }
        public ActionResult Edit(Guid reportId)
        {
            var report = _taskReportService.ShowTaskReport(reportId);
            var viewModel = Mapper.Map<TaskReportViewModel>(report);
            return View(viewModel);
        }
        [HttpPost]
        public ActionResult Edit(TaskReportViewModel taskreport)
        { 
            var domainreport = Mapper.Map<TaskReport>(taskreport);
            _taskReportService.EditTaskReport(domainreport);
            return RedirectToAction("Description", new { reportId = domainreport.Id });
        }

    }
}