﻿using AutoMapper;
using MyTask.Domain.Models;
using MyTask.Domain.Repositories;
using MyTask.Domain.Services;
using MyTask.UI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MyTask.UI.Controllers
{
    public class TasksController : Controller
    {
        // GET: Tasks
        private readonly ITasksService _tasksService;
        public TasksController(ITasksService tasksService)
        {
            _tasksService = tasksService;
        }
        public PartialViewResult Index(Guid id)
        {
            var tasksList = _tasksService.ShowTasks(id);

            var viewModel = Mapper.Map<IEnumerable<TasksViewModel>>(tasksList);

            return PartialView(viewModel);
        }
        public ActionResult Description(Guid id)
        {
            var tasks = _tasksService.ShowTaskDecription(id); 
            var viewModel = Mapper.Map<TasksViewModel>(tasks);

            return View(viewModel);
        }

      
    }
}