﻿using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using MyTask.UI.Authentication;
using MyTask.UI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace MyTask.UI.Controllers
{
    public class LoginController : Controller
    {
        private UserManager<UserViewModel, string> _userManager;

        private IAuthenticationManager AuthenticationManager => HttpContext.GetOwinContext().Authentication;

        public LoginController(UserManager<UserViewModel, string> userManager)
        {
            _userManager = userManager;

        }

        public PartialViewResult Login()
        {
            return PartialView(new LoginViewModel());
        }

        [HttpPost]
        public async Task<ActionResult> Login(LoginViewModel login)
        {

            var user = await _userManager.FindAsync(login.UserName, login.Password);
            if (user == null)
            {
                return View("Unauthorized");
            }

            var identity = await _userManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
            AuthenticationManager.SignIn(identity);

            return RedirectToAction("Index", "Home");
        }

        [Authorize]
        public ActionResult Logout()
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);

            return RedirectToAction("Index", "Home");
        }

        public ActionResult Unauthorized(string returnUrl)
        {
            return View();
        }

    }
}