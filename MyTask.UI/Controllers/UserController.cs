﻿using AutoMapper;
using MyTask.Domain.Repositories;
using MyTask.Domain.Services;
using MyTask.UI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MyTask.UI.Controllers
{
    public class UserController : Controller
    {
        private readonly IUserService _userService;
        public UserController(IUserService userService)
        {
            _userService = userService;
        }
        public ActionResult Index(Guid id)
        {
            var user = _userService.ShowUser(id);
            var viewModel = Mapper.Map<FullUserViewModel>(user);
            return View(viewModel);
        }
    }
}