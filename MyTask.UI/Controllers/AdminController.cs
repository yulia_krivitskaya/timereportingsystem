﻿using AutoMapper;
using MyTask.Domain.Models;
using MyTask.Domain.Repositories;
using MyTask.Domain.Services;
using MyTask.UI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MyTask.UI.Controllers
{
    //[Authorize(Roles = "Admin")]
    public class AdminController : Controller
    {
        private readonly IProjectService _projectService;
        private readonly ITasksService _taskService;
        public AdminController(IProjectService projectService, ITasksService taskService)
        {
            _projectService = projectService;
            _taskService = taskService;
        }
        public ActionResult Index()
        {
            var projectList = _projectService.EditProjects();
            var viewModel = Mapper.Map<IEnumerable<ProjectViewModel>>(projectList);
            return View(viewModel);
        }
        public ActionResult EditProject(Guid id)
        {
            var project = _projectService.ShowProjectDecription(id);
            var viewModel = Mapper.Map<ProjectViewModel>(project);
            return View(viewModel);
        }

        [HttpPost]
        public ActionResult EditProject(ProjectViewModel project)
        {
            var domainProject = Mapper.Map<Project>(project);
            _projectService.EditProject(domainProject);
            return RedirectToAction("Index");
        }

        public ActionResult CreateProject()
        {
            
            return View();
        }

        [HttpPost]
        public ActionResult CreateProject(ProjectViewModel project)
        {
            var newProject = Mapper.Map<Project>(project);
            _projectService.CreateProject(newProject);
            return RedirectToAction("Index");
        }
        [HttpGet]
        public ActionResult CreateTask(Guid id)
        {
            var task = new TasksViewModel();
            task.ProjectId = id;
            return View(task);
        }

        [HttpPost]
        public ActionResult CreateTask(TasksViewModel taskvm)
        {
            var task = Mapper.Map<Tasks>(taskvm);
            _taskService.Create(task);
            return RedirectToAction("Index");

        }
    }
}