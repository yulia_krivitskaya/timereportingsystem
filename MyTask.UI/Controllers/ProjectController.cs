﻿using AutoMapper;
using MyTask.Domain.Repositories;
using MyTask.Domain.Services;
using MyTask.UI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MyTask.UI.Controllers
{
    public class ProjectController : Controller
    {
        private readonly IProjectService _projectService;
        public ProjectController(IProjectService projectService)
        {
            _projectService = projectService;
        }
        public ActionResult Index()
        {
            var projectList = _projectService.ShowAvaliableProjects();

            var viewModel = Mapper.Map<IEnumerable<ProjectViewModel>>(projectList);

            return View(viewModel);
        }
        public ActionResult Description(Guid id)
        {
            var project = _projectService.ShowProjectDecription(id);
            var viewModel = Mapper.Map<ProjectViewModel>(project);

            return View(viewModel);
        }
    }
}