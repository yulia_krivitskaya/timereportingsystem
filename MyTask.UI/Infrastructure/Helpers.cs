﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;

namespace MyTask.UI.Infrastructure
{
    public static class Helpers
    {
        public static MvcHtmlString DatepickerFor<TModel>(this HtmlHelper<TModel> helper, Expression<Func<TModel, DateTime>> expression, object attributes)
        {
            var name = ExpressionHelper.GetExpressionText(expression);

            var metadata = ModelMetadata.FromLambdaExpression(expression, helper.ViewData);
            var fullHtmlFieldName = helper.ViewContext.ViewData.TemplateInfo.GetFullHtmlFieldName(name);

            var tagBuilder = new TagBuilder("input");
            var attributesDictionary = HtmlHelper.AnonymousObjectToHtmlAttributes(attributes);
            tagBuilder.GenerateId(fullHtmlFieldName);
            tagBuilder.MergeAttributes(attributesDictionary);
            tagBuilder.MergeAttribute("name", fullHtmlFieldName, true);
            tagBuilder.MergeAttribute("data-provide", "datepicker");
            tagBuilder.MergeAttribute("data-format", "MM/dd/yyyy");

            ModelState state;
            helper.ViewData.ModelState.TryGetValue(fullHtmlFieldName, out state);
            var date = state?.Value == null ? $"{metadata.Model:MM/dd/yyyy}" : state.Value.AttemptedValue;

            tagBuilder.MergeAttribute("value", date);

            return MvcHtmlString.Create(tagBuilder.ToString());
        }
    }
}